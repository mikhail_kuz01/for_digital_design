function main() {
    let arr = new Array(20);
    for (let i = 0; i < arr.length; i++) {
        arr[i] = getRundomInt(100); 
    } 
    document.getElementById("start_mas").innerHTML = arr;
    
    replaceFromArray(arr);
    document.getElementById("replace_mas").innerHTML = arr;

    addToArray(arr);
    document.getElementById("add_mas").innerHTML = arr;
}

function getRundomInt(n) {
    let koef = Math.pow(-1, Math.round(Math.random() * 10));
    return koef * Math.round(n * Math.random());
}

function replaceFromArray(mas) {
    for (let j = 0; j < mas.length; j++) {
        if (mas[j] < 0) mas[j] = 0;
    }
    return mas;
}

function addToArray(mas) {
    let flag = 0;
    for(let i = 0; i < mas.length; i++) {
        if (mas[i] == 0) {
            flag++;
            if (flag == 2) {
                let len = mas.length;
                mas.push(mas[len-1]);
                for (len; len > i+1; len--) 
                    mas[len-1] = mas[len-2];
                
                mas[i+1] = -1;
                flag = 0;
            } 
        }
    }
    return mas;
}
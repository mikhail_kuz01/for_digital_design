const res = model.reduce((lastData, {user: currentUser, ...currentPost}) => {
	const indexUserInList = lastData.findIndex(user => user.id === currentUser.id);

    if (indexUserInList !== -1) {
    	lastData[indexUserInList].posts.push(currentPost);
    } else {
    	lastData.push({
      	    ...currentUser,
            posts: [currentPost]
        })
    }

    return lastData;
}, []);

console.log('res', res);